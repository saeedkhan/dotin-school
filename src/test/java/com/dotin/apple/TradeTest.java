package com.dotin.apple;

import com.dotin.trade.Trader;
import com.dotin.trade.TradingService;
import com.dotin.trade.Transaction;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class TradeTest {
    private TradingService ts = TradingService.get();

    Trader raoul = new Trader("Raoul", "Cambridge");
    Trader mario = new Trader("Mario","Milan");
    Trader alan = new Trader("Alan","Cambridge");
    Trader brian = new Trader("Brian","Cambridge");
    Transaction trans1 = new Transaction(brian, 2011, 300);
    Transaction trans2 = new Transaction(raoul, 2012, 1000);
    Transaction trans3 = new Transaction(raoul, 2011, 400);
    Transaction trans4 = new Transaction(mario, 2012, 710);
    Transaction trans5 = new Transaction(mario, 2012, 700);
    Transaction trans6 = new Transaction(alan, 2012, 950);
    List<Transaction> transactions = Arrays.asList(
            trans1,
            trans2,
            trans3,
            trans4,
            trans5,
            trans6
    );

    @Test
    public void test_get2011transact(){
        List<Transaction> transacts = ts.get2011transact(transactions);
        assertThat(transacts, hasSize(2));
        assertThat(transacts, contains(trans1, trans3));
    }

    @Test
    public void test_getUniqueCities(){
        assertThat(ts.getUniqueCities(transactions), hasSize(2));
        assertThat(ts.getUniqueCities(transactions), containsInAnyOrder("Cambridge", "Milan"));
    }

    @Test
    public void test_getTradersCambridge(){
        assertThat(ts.getTradersCambridge(transactions), hasSize(3));
        assertThat(ts.getTradersCambridge(transactions), contains(alan, brian, raoul));
    }

    @Test
    public void test_getTradersSorted(){
        assertThat(ts.getTradersSorted(transactions),
                equalToIgnoringWhiteSpace("Alan Brian Mario Raoul"));
    }

    @Test
    public void test_getMilanTrader(){
        assertThat(ts.getMilanTrader(transactions), equalTo(true));
    }

    @Test
    public void test_printTransactValue(){
        assertThat(ts.getMilanTrader(transactions), equalTo(true));
    }

    @Test
    public void test_getHighestValue(){
        assertThat(ts.getHighestValue(transactions), equalTo(1000));
    }

    @Test
    public void test_getSmallestTransact(){
        assertThat(ts.getSmallestTransact(transactions), equalTo(trans1));
    }









}
