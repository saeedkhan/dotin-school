package com.dotin.apple;

import com.sun.xml.internal.ws.policy.AssertionSet;
import org.junit.Test;
import static org.hamcrest.Matchers.*;

import java.util.*;

import static org.hamcrest.MatcherAssert.*;

public class DishTest {

    Dish dish1 = new Dish("pork", false, 800, Dish.Type.MEAT);
    Dish dish2 = new Dish("beef", false, 700, Dish.Type.MEAT);
    Dish dish3 = new Dish("chicken", false, 400, Dish.Type.MEAT);
    Dish dish4 = new Dish("french fries", true, 530, Dish.Type.OTHER);
    Dish dish5 = new Dish("rice", true, 350, Dish.Type.OTHER);
    Dish dish6 = new Dish("season fruit", true, 120, Dish.Type.OTHER);
    Dish dish7 = new Dish("pizza", true, 550, Dish.Type.OTHER);
    Dish dish8 = new Dish("prawns", false, 300, Dish.Type.FISH);
    Dish dish9 = new Dish("salmon", false, 450, Dish.Type.FISH);

    List<Dish> menu = Arrays.asList(dish1, dish2, dish3, dish4, dish5, dish6, dish7, dish8, dish9);

    DishService dishService = DishService.get();

    @Test
    public void test_findMaxCalDish(){
        assertThat(dishService.findMaxCalDish(menu), equalTo(dish1));
    }

    @Test
    public void test_summingDishes(){
        assertThat(dishService.summingDishes(menu), equalTo((long) menu.stream().mapToInt(Dish::getCalories).sum()));
    }

    @Test
    public void test_intSummaryStatistics(){
        long sum = (long) menu.stream().mapToInt(Dish::getCalories).sum();
        assertThat(dishService.getSummaryStatistics(menu).getSum(), equalTo(sum));
        assertThat((long)dishService.getSummaryStatistics(menu).getAverage(), equalTo(sum/menu.size()));
    }

//    ?????????????????????????????????????????????????
//    ???????????????????????????????????????????????????
    @Test
    public void test_grouper(){
//        assertThat();
    }

    @Test
    public void test_sumOfCalories(){
        assertThat(dishService.sumOfCalories(menu), equalTo(4200));
    }

    @Test
    public void test_highestCaloryByType(){
        HashMap<Dish.Type, Dish> typeDishHashMap = new HashMap<>();
        typeDishHashMap.put(dish1.getType(), dish1);
        typeDishHashMap.put(dish7.getType(), dish7);
        typeDishHashMap.put(dish9.getType(), dish9);
        assertThat(dishService.highestCaloryByType(menu), equalTo(typeDishHashMap));
    }

    @Test
    public void test_caloricLevelsByType(){
        Map<Dish.Type, Set<DishServiceImp.CaloryLevel>> expected = new HashMap<>();

        Set<DishServiceImp.CaloryLevel> MeatCaloryLevels = new HashSet<DishServiceImp.CaloryLevel>();
        MeatCaloryLevels.addAll(Arrays.asList(DishServiceImp.CaloryLevel.DIET
                , DishServiceImp.CaloryLevel.NORMAL
                , DishServiceImp.CaloryLevel.FAT));

        Set<DishServiceImp.CaloryLevel> OtherCaloryLevels = new HashSet<DishServiceImp.CaloryLevel>();
        OtherCaloryLevels.addAll(Arrays.asList(DishServiceImp.CaloryLevel.DIET
                , DishServiceImp.CaloryLevel.NORMAL));

        Set<DishServiceImp.CaloryLevel> FishCaloryLevels = new HashSet<DishServiceImp.CaloryLevel>();
        FishCaloryLevels.addAll(Arrays.asList(DishServiceImp.CaloryLevel.DIET
                , DishServiceImp.CaloryLevel.NORMAL));

        expected.put(Dish.Type.MEAT
                , MeatCaloryLevels);
        expected.put(Dish.Type.OTHER
                , OtherCaloryLevels);
        expected.put(Dish.Type.FISH
                , FishCaloryLevels);

        assertThat(dishService.CaloricLevelsByType(menu), equalTo(expected));
    }

    @Test
    public void test_vegiNonVegi(){

        List<Dish> vegi = new ArrayList<>();
        vegi.addAll(Arrays.asList(dish4, dish5, dish6, dish7));

        assertThat(dishService.vegiAndNonVegi(menu).get(true)
                , equalTo(vegi));
    }

    @Test
    public void test_mostCaloricPartionedByVegeterian(){
        Map<Boolean, Dish> mostCaloric = new HashMap<>();
        mostCaloric.put(false, dish1);
        mostCaloric.put(true, dish7);

        assertThat(dishService.mostCaloricPartitionedByVegeterian(menu), equalTo(mostCaloric));
    }


}
