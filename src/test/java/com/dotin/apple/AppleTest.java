package com.dotin.apple;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import org.junit.Test;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class AppleTest {

    private AppleService appleService = AppleService.get();

    @Test
    public void sucess_format_apple_fancy(){
        List<Apple> lap = new ArrayList();
        lap.add(new Apple(151, "red"));
        lap.add(new Apple(60, "blue"));
        lap.add(new Apple(75, "yellow"));

        List<String> formattedStrings = appleService.formatApples(lap);

        assertThat(formattedStrings.size(), equalTo(lap.size()));
        assertThat(formattedStrings, containsInAnyOrder(
                "A heavy red apple"
                , "A light blue apple"
                , "A light yellow apple"
        ));
    }

    @Test
    public void success_test_collection_matchers(){
        List<Integer> list = Arrays.asList(2, 5, 4);
        assertThat(list, hasSize(3));
        assertThat(list, containsInAnyOrder(2,4,5));
        assertThat(list, everyItem(is(greaterThan(1))));
    }

    @Test
    public void array_test(){
        Integer[] ints = new Integer[]{ 7, 5, 12, 16};
        assertThat(ints, arrayWithSize(4));
        assertThat(ints, arrayContainingInAnyOrder(16, 12, 7, 5));
    }

    @Test
    public void test_apple_sorter(){
        List<Apple> lap = new ArrayList<>();
        lap.add(new Apple(120));
        lap.add(new Apple(95));
        lap.add(new Apple(100));

        appleService.appleSorter(lap);

        assertThat(new Integer[]{lap.get(0).getWeight(),lap.get(1).getWeight(), lap.get(2).getWeight()}, arrayContaining(95, 100, 120));
    }

    @Test
    public void test_apple_file_process(){
        String apString = appleService.fileProcess((BufferedReader bf)-> bf.readLine());
        assertThat(apString, equalTo("color: yellow weight: 100"));
    }

    @Test
    public void test_non_empty_finder(){
        List<String> sts = new ArrayList<>();
        sts.add("blue");
        sts.add("");
        sts.add("yellow");
        sts.add("");
        sts.add("black");

        List<String> result = appleService.nonEmptyFinder(sts, (String s) -> !s.isEmpty());

        assertThat(result, hasSize(3));
        assertThat(result, contains("blue", "yellow", "black"));
    }

    @Test
    public void test_apple_mapper(){
        List<String> sts = new ArrayList<>();
        sts.add("blue");
        sts.add("yellow");
        sts.add("");
        sts.add("red");

        List<Integer> result = appleService.mapper(sts, (String s)-> s.length());

        assertThat(result, hasSize(4));
        assertThat(result, contains(4, 6, 0, 3));
    }

    @Test
    public void test_apple_map(){
        List<String> sts = new ArrayList<>();
        sts.add("blue");
        sts.add("yellow");
        sts.add("red");

        List<Apple> apples = appleService.map(sts, (String s)-> new Apple(s));

        assertThat(apples, hasSize(3));
        assertThat(apples.get(0).getColor(), equalTo("blue"));
        assertThat(apples.get(1).getColor(), equalTo("yellow"));
        assertThat(apples.get(2).getColor(), equalTo("red"));
    }

    @Test
    public void test_fruit_factory(){
        Apple app = (Apple)appleService.fruitFactory("apple", 120);
        assertThat(app.getWeight(), equalTo(120));
        assertThat(app.getColor(), equalTo("white"));
    }
}