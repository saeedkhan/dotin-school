package com.dotin.apple;

import org.junit.Test;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class DateTimeTest {

    DateService dateService = DateService.get();

    @Test
    public void test_getNextWrokingDay(){
        assertThat(LocalDate.of(2018, 7, 24)
                , equalTo(dateService.getNextWorkingDay(LocalDate.of(2018, 7, 23))));
        assertThat(LocalDate.of(2018, 7, 30)
                , equalTo(dateService.getNextWorkingDay(LocalDate.of(2018, 7, 27))));
        assertThat(LocalDate.of(2018, 7, 30)
                , equalTo(dateService.getNextWorkingDay(LocalDate.of(2018, 7, 28))));
    }
    @Test
    public void test_getNextWrokingDayV2(){
        assertThat(LocalDate.of(2018, 7, 24)
                , equalTo(dateService.getNextWorkingDayV2(LocalDate.of(2018, 7, 23))));
        assertThat(LocalDate.of(2018, 7, 30)
                , equalTo(dateService.getNextWorkingDayV2(LocalDate.of(2018, 7, 27))));
        assertThat(LocalDate.of(2018, 7, 30)
                , equalTo(dateService.getNextWorkingDayV2(LocalDate.of(2018, 7, 28))));
    }

    @Test
    public void test_dateFormatter(){
        assertThat("20180714", equalTo(dateService.dateFormatter(LocalDate.of(2018, 7, 14))));
    }

    @Test
    public void test_dateParser(){
        assertThat(LocalDate.of(2018, 7, 14), equalTo(dateService.dateParser("20180714")));
    }

}
