package com.dotin.xmlProcessor;

import org.junit.Test;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class BasketGeneratorTest {

    BasketGenerator basketGenerator = new BasketGenerator();

    @Test
    public void randomAppleGenerator_test(){
        assertThat(basketGenerator.generateRandomApple().getClass(), equalTo(new Apple().getClass()));
    }

    @Test
    public void generateBasket_test(){
        assertThat(basketGenerator.generateBasket(5).getClass(), equalTo(new Basket().getClass()));
        assertThat(basketGenerator.generateBasket(5).getApples().size(), equalTo(5));
    }
}
