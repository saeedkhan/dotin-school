package com.dotin.xmlProcessor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement (name = "basket")
public class Basket {

    @XmlElement(name = "apple")
    List<Apple> apples;

    public List<Apple> getApples() {
        return apples;
    }

//    public void setApples(List<Apple> apples) {
//        this.apples = apples;
//    }

    public void add(Apple apple) {
        try {
            if (apples == null) {
                apples = new ArrayList<>();
            }
            apples.add(apple);
        } catch ( Exception e) {
        }
    }
}
