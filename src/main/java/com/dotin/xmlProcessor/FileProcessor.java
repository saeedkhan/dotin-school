package com.dotin.xmlProcessor;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class FileProcessor {

    public static void marshalling(List<Basket> baskets, String filePath) {
        long start = System.nanoTime();
        for (int i = 0; i < baskets.size(); i++) {
            marshalBasketAsCompletableFuture(baskets.get(i), filePath + "basket" + (i + 1) + ".xml");
        }
        System.out.println("marshalling time: " + (System.nanoTime() - start) / 1000);
    }

    public static void marshalBasketAsCompletableFuture(Basket basket, String filePath) {
        try {
            JAXBContext jContext = JAXBContext.newInstance(Basket.class);
            Marshaller marshaller = jContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            CompletableFuture.runAsync(()->{
                try{
                    marshaller.marshal(basket, new FileOutputStream(filePath));
                }catch (Exception e){
                    e.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<CompletableFuture<Basket>> unMarshalling() {
        List<CompletableFuture<Basket>> futureBaskets = new ArrayList<>();
        File[] files = new File("src/main/resources/baskets/").listFiles();

        long start = System.nanoTime();
        for (File file : files) {
            futureBaskets.add(CompletableFuture.supplyAsync(() -> unMarshalBasketAsCompletableFuture(file)));
        }
        System.out.println("unmarshalling time:" + (System.nanoTime() - start) / 1000);

        return futureBaskets;
    }

    public static Basket unMarshalBasketAsCompletableFuture(File file) {
        Basket basket = null;
        try {
            JAXBContext jContext = JAXBContext.newInstance(Basket.class);
            Unmarshaller unmarshaller = jContext.createUnmarshaller();
            basket = (Basket) unmarshaller.unmarshal(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return basket;
    }
}
