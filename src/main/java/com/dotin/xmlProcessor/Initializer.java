package com.dotin.xmlProcessor;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class Initializer {
    public static void initialize(){
        BasketGenerator basketGenerator = new BasketGenerator();
        List<CompletableFuture<Basket>> futureBaskets = new ArrayList<>();

        long start = System.nanoTime();
        for(int i = 1; i <= 20; i++){
            futureBaskets.add(CompletableFuture.supplyAsync(()->basketGenerator.generateBasket(40)));
        }
        System.out.println("generating baskets: " + (System.nanoTime()-start)/1000);

        FileProcessor fileProcessor = new FileProcessor();
        fileProcessor.marshalling(futureBaskets.stream().map(CompletableFuture::join).collect(Collectors.toList()),
                "src/main/resources/baskets/");
    }
}
