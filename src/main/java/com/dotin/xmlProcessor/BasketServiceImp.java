package com.dotin.xmlProcessor;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.*;

public class BasketServiceImp implements BasketService {

    public Map<String, Long> getNumberOfApplesByColorAsString(){
        long start = System.nanoTime();
        Map<String, Long> map =
                FileProcessor.unMarshalling().stream()
                        .flatMap(basket -> basket.join().getApples().stream())
                        .collect(groupingBy(Apple::getColor, counting()));

        long duration = (System.nanoTime()-start)/1000;
        System.out.println("apples by color duration: " + duration);

        return map;
    }

    public Map<Quality, Long> getNumberOfApplesByQuality(){
        long start = System.nanoTime();
        Map<Quality, Long> map = FileProcessor.unMarshalling().stream()
                .flatMap(basket -> basket.join().getApples().stream())
                .collect(groupingBy(apple->{
                        if(apple.getWeight()<100) return Quality.LOW;
                        else if(apple.getWeight()<150) return Quality.MEDIUM;
                        else return Quality.HIGHT;
                            }, counting()));

        long duration = (System.nanoTime()-start)/1000;
        System.out.println("apples by quality duration: " + duration);

        return map;
    }
}
