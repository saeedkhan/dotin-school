package com.dotin.xmlProcessor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (name="apple")
public class Apple {

    private int weight;
    private String color;

    public Apple() {
    }

    public Apple(String color, int weight) {

        this.weight = weight;
        this.color = color;
    }

    @XmlElement  (name="weight")
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @XmlElement (name="color")
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
