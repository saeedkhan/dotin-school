package com.dotin.xmlProcessor;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class BasketGenerator {

    Random random = new Random();
    List<String> colors = Arrays.asList("red", "yellow", "green");

    public Apple generateRandomApple() {
        String randomColor = colors.get(random.nextInt(3));
        int randomWeight = random.nextInt(150) + 50;
        Apple apple = new Apple(randomColor, randomWeight);
        return apple;
    }

    public Basket generateBasket(int size) {
        Basket basket = new Basket();
        for (int i = 0; i < size; i++) {
            Apple apple = generateRandomApple();
            basket.add(apple);
        }
        return basket;
    }
}
