package com.dotin.xmlProcessor;

import java.util.Map;

public interface BasketService {

    public static BasketService get(){
        return new BasketServiceImp();
    }

    public Map<String, Long> getNumberOfApplesByColorAsString();
    public Map<Quality, Long> getNumberOfApplesByQuality();
    }
