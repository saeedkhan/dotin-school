package com.dotin.trade;

import java.util.ArrayList;
import java.util.List;

public interface TradingService {

    static TradingService get(){
        return new TradingServiceImp();
    }

    public List<Transaction> get2011transact(List<Transaction> transactions);
    public List<String> getUniqueCities(List<Transaction> transactions);
    public List<Trader> getTradersCambridge(List<Transaction> transactions);
    public String getTradersSorted(List<Transaction> transactions);
    public Boolean getMilanTrader(List<Transaction> transactions);
    public void printTransactValue(List<Transaction> transactions);
    public Integer getHighestValue(List<Transaction> transactions);
    public Transaction getSmallestTransact(List<Transaction> transactions);


    }
