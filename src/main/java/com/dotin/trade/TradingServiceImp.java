package com.dotin.trade;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

public class TradingServiceImp implements TradingService {

    public List<Transaction> get2011transact(List<Transaction> transactions){
        List<Transaction> transacts = transactions.stream()
                .filter(d->d.getYear()==2011)
                .sorted(comparing(Transaction::getValue))
                .collect(Collectors.toList());
        return transacts;
    }
    public List<String> getUniqueCities(List<Transaction> transactions){
        List<String> uniqueCities = transactions.stream()
                .map(t->t.getTrader().getCity())
                .distinct()
                .collect(Collectors.toList());
        return uniqueCities;
    }
    public List<Trader> getTradersCambridge(List<Transaction> transactions){
        List<Trader> trdsCamb = transactions.stream()
                .map(Transaction::getTrader)
                .filter(t-> t.getCity().equals("Cambridge"))
                .distinct()
                .sorted(comparing(Trader::getName))
                .collect(Collectors.toList());
        return trdsCamb;
    }
    public String getTradersSorted(List<Transaction> transactions){
        String trdsSorted = transactions.stream()
                .map(t->t.getTrader().getName())
                .distinct()
                .sorted()
                .reduce("", (t1, t2)-> t1+ " " +t2);
        return trdsSorted;
    }

    public Boolean getMilanTrader(List<Transaction> transactions){
        Boolean milanTrader = transactions.stream()
                .anyMatch(t->t.getTrader()
                        .getCity()
                        .equalsIgnoreCase("Milan"));
        return milanTrader;
    }
    public void printTransactValue(List<Transaction> transactions){
        transactions.stream()
                .filter(i-> i.getTrader().getCity().equalsIgnoreCase("cambridge"))
                .map(Transaction::getValue)
                .forEach(System.out::println);
    }
    public Integer getHighestValue(List<Transaction> transactions){
        Integer highestValue = transactions.stream()
                .map(Transaction::getValue)
                .reduce(0, Integer::max);
        return highestValue;
    }
    public Transaction getSmallestTransact(List<Transaction> transactions){
        Transaction smallestValue = transactions.stream()
                .reduce((i,j)->i.getValue()<j.getValue()?i:j)
                .get();
        return smallestValue;
    }




}
