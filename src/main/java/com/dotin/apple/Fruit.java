package com.dotin.apple;

import javafx.beans.DefaultProperty;

abstract class Fruit {

    private int weight;
    private String color;

    public Fruit() {
        this.weight = 0;
        this.color = "";
    }

    public Fruit(int weight) {
        this.weight = weight;
        this.color = "white";
    }

    public Fruit(String color) {
        this.color = color;
        this.weight = 100;
    }

    public Fruit(int weight, String color) {
        this.weight = weight;
        this.color = color;
    }

    public Integer getWeight() {
        return weight;
    }

    public String getColor() {
        return color;
    }

    public void setWeight(int weight) {

        this.weight = weight;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
