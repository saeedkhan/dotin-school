package com.dotin.apple;

import java.time.LocalDate;
import java.time.temporal.Temporal;

public interface DateService {

    Temporal getNextWorkingDay(Temporal temp);
    Temporal getNextWorkingDayV2(Temporal temp);
    public String dateFormatter(LocalDate localDate);
    public LocalDate dateParser(String date);



        public static DateService get(){
        return new DateServiceImp();
    }
}
