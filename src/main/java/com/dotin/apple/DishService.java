package com.dotin.apple;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;

public interface DishService {

    Dish findMaxCalDish(List<Dish> menu);
    long summingDishes(List<Dish> menu);
    IntSummaryStatistics getSummaryStatistics(List<Dish> menu);
    Map grouper(List<Dish> menu);
    int sumOfCalories(List<Dish> menu);
    Map highestCaloryByType(List<Dish> menu);
    Map CaloricLevelsByType(List<Dish> menu);
    Map vegiAndNonVegi(List<Dish> menu);
    Map mostCaloricPartitionedByVegeterian(List<Dish> menu);


        public static DishService get(){
        return new DishServiceImp();
    }
}
