package com.dotin.apple;

public class Apple extends Fruit {
    public Apple() {
    }

    public Apple(int weight) {
        super(weight);
    }

    public Apple(String color) {
        super(color);
    }

    public Apple(int weight, String color) {
        super(weight, color);
    }
}
