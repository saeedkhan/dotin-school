package com.dotin.apple;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.*;

public class ShopService {


    List<Shop> shops = Arrays.asList(new Shop("best shop")
            , new Shop("bestBuy")
            , new Shop("target")
            , new Shop("wallmart"));

    private final Executor executor = Executors.newFixedThreadPool(Math.min(100, shops.size()),
            new ThreadFactory() {
                @Override
                public Thread newThread(Runnable r) {
                    Thread t = new Thread(r);
                    t.setDaemon(true);
                    return t;
                }
            });


    public List<String> findPricesParallelStreams(String product){
        return shops.parallelStream().map(shop -> String.format("%s price is: %.2f"
                ,shop.getName(),shop.getPrice(product))).collect(toList());
    }

    public List<String> findPricesConcurrent(String product){
        List<CompletableFuture<String>> shopsList =  shops.stream().map(shop -> CompletableFuture.supplyAsync(()-> String.format("%s price is: %.2f"
                ,shop.getName(), shop.getPrice(product)), executor)).collect(toList());
        return shopsList.stream().map(CompletableFuture::join).collect(toList());
    }

}
