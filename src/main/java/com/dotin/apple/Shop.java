package com.dotin.apple;

import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public class Shop {

    private String name;
    public Shop(String name){
        this.name = name;
    }

    private Random random = new Random();

    public double getPrice(String product){
        return calculatePrice(product);
    }
//
//    public String getPriceString(String product){
//        double price = calculatePrice(product);
//        String code = random.nextInt();
//        return;
//    }

    public Future<Double> getPriceAsync(String product){

        CompletableFuture futurePrice = CompletableFuture.supplyAsync(()-> calculatePrice(product));
        return futurePrice;
    }

    public double calculatePrice(String product){
        delay();
        return product.charAt(1) + product.charAt(1) * random.nextDouble();
    }

    public void delay(){
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e){
            throw new RuntimeException(e);
        }
    }

    public String getName(){
        return name;
    }

}
