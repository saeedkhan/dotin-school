package com.dotin.apple;

public class Banana extends Fruit {
    public Banana() {
    }

    public Banana(int weight) {
        super(weight);
    }

    public Banana(String color) {
        super(color);
    }

    public Banana(int weight, String color) {
        super(weight, color);
    }
}
