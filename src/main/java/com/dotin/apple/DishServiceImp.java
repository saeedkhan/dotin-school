package com.dotin.apple;

import com.sun.javafx.collections.MappingChange;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;
import static java.util.Comparator.comparing;

public class DishServiceImp implements DishService {

    public void dishService(List<Dish> menu) {
        List<String> names = menu.stream()
                .filter(d -> {
                    System.out.println("filtering: " + d.getName());
                    return d.getCalories() > 400;
                })
                .limit(3)
                .distinct()
                .sorted(comparing(Dish::getName))
                .map(dish -> {
                    System.out.println("mapping: " + dish.getName());
                    return dish.getName();
                })
                .collect(Collectors.toList());

        List<Dish> mDish = menu.stream().filter(dish -> dish.getType() == Dish.Type.MEAT).limit(2).collect(Collectors.toList());
        List<String> iDish = menu.stream().map(Dish::getName).distinct().map(s -> s.split("")).flatMap(Arrays::stream).distinct().sorted((s1, s2) -> s1.compareTo(s2)).collect(Collectors.toList());
        iDish.stream().forEach(System.out::println);

        List<Integer> ints = Arrays.asList(1, 3, 5, 7);
        List<Integer> ints2 = ints.stream().map(i -> i * i).collect(Collectors.toList());
        ints2.stream().forEach(System.out::println);

        List<Integer> l1 = Arrays.asList(1, 4);
        List<Integer> l2 = Arrays.asList(2, 3, 5);
        List<int[]> pairs = l1.stream()
                .flatMap(num1 -> l2.stream().map(num2 -> new int[]{num1, num2}).filter(num3 -> (num3[0] + num3[1]) % 3 == 0)).collect(Collectors.toList());
        pairs.stream().forEach(i -> System.out.println(i[0] + " " + i[1]));

        //Reduce
        Integer num = menu.stream().map(t -> 1).reduce(0, Integer::sum);

    }

    public Dish findMaxCalDish(List<Dish> menu) {
        Optional<Dish> maxCalloryDish = menu.stream().collect(Collectors.maxBy(Comparator.comparingInt(Dish::getCalories)));
        return maxCalloryDish.orElse(new Dish("", false, 0, Dish.Type.OTHER));
    }

    public long summingDishes(List<Dish> menu) {
        long sumOfCallory = menu.stream().collect(summingInt(Dish::getCalories));
        return sumOfCallory;
    }

    public IntSummaryStatistics getSummaryStatistics(List<Dish> menu) {
        IntSummaryStatistics intSummaryStatistics = menu.stream()
                .collect(Collectors.summarizingInt(Dish::getCalories));
        return intSummaryStatistics;
    }

    public int sumOfCalories(List<Dish> menu){
        int sum = menu.stream().mapToInt(Dish::getCalories).sum();
//        int sum = menu.stream().collect(reducing(0, Dish::getCalories, (c1, c2)->c1+c2));
//        int sum = menu.stream().map(Dish::getCalories).reduce(0, (d1, d2)-> d1+d2);
        return sum;
    }

    public enum CaloryLevel {DIET, NORMAL, FAT}

    public Map grouper(List<Dish> menu) {
        Map<Dish.Type, Map<CaloryLevel, List<Dish>>> mappedMenu = menu.stream()
                .collect(groupingBy(Dish::getType
                        , groupingBy(dish -> {
                            if (dish.getCalories() <= 400) return CaloryLevel.DIET;
                            else if (dish.getCalories() <= 700) return CaloryLevel.NORMAL;
                            else return CaloryLevel.FAT;
                        })));
        return mappedMenu;
    }

    public Map highestCaloryByType(List<Dish> menu){
        Map<Dish.Type, Dish> maxCaloryDishes = menu.stream()
                .collect(groupingBy(Dish::getType, collectingAndThen(maxBy(comparing(Dish::getCalories)), Optional::get)));
        return maxCaloryDishes;
    }

    public Map CaloricLevelsByType(List<Dish> menu){
        Map<Dish.Type, Set<CaloryLevel>> caloricLevelsByType = menu.stream()
                .collect(groupingBy(Dish::getType, mapping(dish->{
                    if(dish.getCalories()<=400) return CaloryLevel.DIET;
                    else if(dish.getCalories()<=700) return CaloryLevel.NORMAL;
                    else return CaloryLevel.FAT;
                }, toSet())));
        return caloricLevelsByType;
    }

    public Map vegiAndNonVegi(List<Dish> menu){
        Map<Boolean, List<Dish>> vegiNonVegi = menu.stream().collect(partitioningBy(Dish::isVegetarian));
        return vegiNonVegi;
    }

    public Map mostCaloricPartitionedByVegeterian(List<Dish> menu){
        Map<Boolean, Dish> mostCaloric = menu.stream()
                .collect(partitioningBy(Dish::isVegetarian, collectingAndThen(maxBy(comparing(Dish::getCalories)), Optional::get)));
        return mostCaloric;
    }

}


