package com.dotin.apple;

import jdk.nashorn.internal.objects.annotations.Constructor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Comparator.comparing;

public class AppleServiceImpl implements AppleService {

    Map<String, Function<Integer, Fruit>> fruitMap = new HashMap();

    AppleServiceImpl() {
        fruitMap.put("apple", Apple::new);
        fruitMap.put("orange", Orange::new);
    }

    @Override
    public List<String> formatApples(List<Apple> apples) {
        List<String> result = new ArrayList<>();
        AppleFancyFormatter appleFancyFormatter = new AppleFancyFormatter();

        for (Apple apple : apples) {
            result.add(appleFancyFormatter.accept(apple));
        }

        return result;
    }

    public void prettyPrintApple(List<Apple> inventory, AppleFormatter formatter) {
        for (Apple apple : inventory) {
            System.out.println(formatter.accept(apple));
        }
    }

//    @Override
//    public void appleSorter(List<Apple> inventory) {
//        inventory.sort(new Comparator<Apple>() {
//
//            public int compare(Apple o1, Apple o2) {
//                return new Integer(o1.getWeight()).compareTo(o2.getWeight());
//            }
//        });
//
//        inventory.sort(
//                (Apple a1, Apple a2) -> new Integer(a1.getWeight()).compareTo(a2.getWeight())
//        );
//
//        for (Apple ap : inventory) {
//            System.out.println(ap.getWeight());
//        }
//    }

    @Override
    public void appleSorter(List<Apple> inventory) {

        inventory.sort(comparing(Apple::getWeight).reversed().thenComparing(Apple::getColor));


    }

    @Override
    public String fileProcess(BufferedReaderProcessor bfr) {
        String apple = "";

        try {
            File file = new File(getClass().getClassLoader().getResource("apples.txt").getFile());
            BufferedReader bf = new BufferedReader(new FileReader(file));
            apple = bfr.process(bf);
        } catch (IOException e) {

        } finally {

        }
        return apple;
    }

    @Override
    public <String> List<String> nonEmptyFinder(List<String> list, Predicate<String> p) {
        List<String> result = new ArrayList<>();
        for (String s : list) {
            if (p.test(s)) {
                result.add(s);
            }
        }
        return result;
    }

    @Override
    public <T> void printer(List<T> list, Consumer<T> c) {
        for (T t : list) {
            c.accept(t);
        }
    }

    @Override
    public <T, R> List<R> mapper(List<T> l, Function<T, R> f) {
        List<R> result = new ArrayList<>();
        for (T t : l) {
            result.add(f.apply(t));
        }
        return result;
    }

    @Override
    public List<Apple> map(List<String> list, Function<String, Apple> f) {
        List<Apple> result = new ArrayList<>();
        for (String s : list) {
            result.add(f.apply(s));
        }
        return result;
    }

    @Override
    public Fruit fruitFactory(String fruitName, int weight) {
        return fruitMap.get(fruitName).apply(weight);
    }
}
