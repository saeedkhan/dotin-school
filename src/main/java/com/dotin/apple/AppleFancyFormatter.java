package com.dotin.apple;

public class AppleFancyFormatter implements AppleFormatter {

    public String accept(Apple ap){
        String characteristic = ap.getWeight()> 150 ? "heavy" : "light";
        return "A " + characteristic + " " + ap.getColor() + " apple";
    }
}
