package com.dotin.apple;

public interface AppleFormatter {
    public String accept(Apple a);
}
