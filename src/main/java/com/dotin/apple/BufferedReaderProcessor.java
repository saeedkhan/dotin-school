package com.dotin.apple;

import java.io.BufferedReader;
import java.io.IOException;

@FunctionalInterface
public interface BufferedReaderProcessor {
    public String process(BufferedReader b) throws IOException;
}
