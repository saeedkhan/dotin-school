package com.dotin.apple;

import com.dotin.xmlProcessor.BasketService;
import com.dotin.xmlProcessor.Initializer;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import static com.dotin.apple.ParallelStreamsService.measuringTime;
import static java.util.Comparator.comparing;

public class Main {

    public static void main(String[] args) {


        Initializer.initialize();
        BasketService basketService = BasketService.get();
        System.out.println(basketService.getNumberOfApplesByColorAsString());
        System.out.println(basketService.getNumberOfApplesByQuality());






/*
        List<Apple> lap = new ArrayList();
        lap.add(new Apple(150, "red"));
        lap.add(new Apple(60, "blue"));
        lap.add(new Apple(75, "yellow"));

        AppleService appleService = AppleService.get();
        appleService.formatApples(lap);

        appleService.appleSorter(lap);

        appleService.fileProcess((BufferedReader bf)->bf.readLine());

        appleService.nonEmptyFinder(new ArrayList<String>(), (String s)-> !s.isEmpty());

        appleService.printer(Arrays.asList(1,2,3,4), i-> System.out.println(i));

        appleService.mapper(Arrays.asList("Hello", "World", "is", "not", "goodbye"), s-> s.length());

        appleService.map(Arrays.asList("green", "red", "yellow"), Apple::new);

        Apple apple3 = (Apple)appleService.fruitFactory("apple", 120);
        Orange orange = (Orange)appleService.fruitFactory("orange", 110);


        Supplier<Apple> supplier = ()-> new Apple();
        Apple apple = supplier.get();

        Function<String, Apple> supplier1 = s -> new Apple(s);
        Apple apple1 = supplier1.apply("green");

        BiFunction<Integer, String, Apple> supplier2 = Apple::new;
        Apple apple2 = supplier2.apply(100 , "blue");

        int poopy = 234;
        Runnable r = ()-> System.out.println(poopy);
*/


//        dishOperations();

//        Stream<int[]> tripleStream =
//                IntStream.rangeClosed(1, 100).boxed()
//                        .flatMap(a-> IntStream.rangeClosed(a, 100)
//                                .filter(b-> Math.sqrt(a*a + b*b)%1 == 0)
//                                .mapToObj(b->new int[]{a, b, (int)Math.sqrt(a * a + b * b)})
//                        );
//
//        Stream<double[]> tripleStream2 = IntStream.rangeClosed(1, 100).boxed()
//                .flatMap(a->IntStream.rangeClosed(a, 100).mapToObj(b-> new double[]{a, b, Math.sqrt(a*a + b*b)}))
//                .filter(t->t[2]%1 == 0);
//
//        tripleStream.forEach(t->System.out.println(t[0] + "," + t[1] + "," + t[2]));
//        tripleStream2.forEach(t->System.out.println(t[0] + "," + t[1] + "," + t[2]));
//
//        Stream<String> lines = Files.lines(Paths.get(getClass().getClassLoader().getResource("apples.txt")));
//        long uniqueWords = lines.flatMap(line->Arrays.stream(line.split(" "))).distinct().count();
//
//        Stream.iterate(new int[]{0, 1}, t-> new int[]{t[1], t[0]+t[1]})
//                .limit(10)
//                .map(t->t[1])
//                .forEach(System.out::println);

        //Date and Time operations
//        DateService dateService = DateService.get();
//        System.out.println(dateService.getNextWorkingDay(LocalDate.now()));


        //summarization operations

//        int i = 1000000;
//        System.out.println("parallel streams: " + measuringTime(ParallelStreamsService::parallelSum, i));
//        System.out.println("sequential streams: " + measuringTime(ParallelStreamsService::sequentialSum, i));
//        System.out.println("iterator: " + measuringTime(ParallelStreamsService::iterator, i));
//        System.out.println("ranged stream: " + measuringTime(ParallelStreamsService::rangedSum, i));
//        System.out.println("ranged parallel stream: " + measuringTime(ParallelStreamsService::rangedParallelSum, i));
//
//
//
//
//
//
//
//
//        Shop shop = new Shop("best shop");
//        long start = System.nanoTime()/1000;
//        Future<Double> futurePrice = shop.getPriceAsync("table");
//        long invocationTime = System.nanoTime()/1000 -start;
//        System.out.println("Invocation returned after: " + invocationTime);
//        double price;
//        try {
//            price = futurePrice.get();
//        }catch (Exception e){
//            throw new RuntimeException(e);
//        }
//        long retrievalTime = System.nanoTime()/1000 - start;
//
//        System.out.println("price: " + price);
//        System.out.println("return time: " + retrievalTime);


    }



    private static void dishOperations(){
        List<Dish> menu = Arrays.asList(
                new Dish("pork", false, 800, Dish.Type.MEAT),
                new Dish("beef", false, 700, Dish.Type.MEAT),
                new Dish("chicken", false, 400, Dish.Type.MEAT),
                new Dish("french fries", true, 530, Dish.Type.OTHER),
                new Dish("rice", true, 350, Dish.Type.OTHER),
                new Dish("season fruit", true, 120, Dish.Type.OTHER),
                new Dish("pizza", true, 550, Dish.Type.OTHER),
                new Dish("prawns", false, 300, Dish.Type.FISH),
                new Dish("salmon", false, 450, Dish.Type.FISH) );

        List<String> names = menu.stream()
                .filter(d -> {
                        System.out.println("filtering: " + d.getName());
                        return d.getCalories()>400;
                })
                .limit(3)
                .distinct()
                .sorted(comparing(Dish::getName))
                .map(dish -> {
                    System.out.println("mapping: " + dish.getName());
                    return dish.getName();
                })
                .collect(Collectors.toList());

        List<Dish> mDish = menu.stream().filter(dish -> dish.getType()==Dish.Type.MEAT).limit(2).collect(Collectors.toList());
        List<String> iDish = menu.stream().map(Dish::getName).distinct().map(s -> s.split("")).flatMap(Arrays::stream).distinct().sorted((s1, s2)-> s1.compareTo(s2)).collect(Collectors.toList());
        iDish.stream().forEach(System.out::println);

        List<Integer> ints = Arrays.asList( 1, 3, 5, 7);
        List<Integer> ints2 = ints.stream().map(i -> i * i).collect(Collectors.toList());
        ints2.stream().forEach(System.out::println);

        List<Integer> l1 = Arrays.asList(1, 4);
        List<Integer> l2 = Arrays.asList(2, 3, 5);
        List<int[]> pairs = l1.stream()
                .flatMap(num1-> l2.stream().map(num2->new int[]{num1,num2}).filter(num3-> (num3[0] + num3[1])%3 == 0)).collect(Collectors.toList());
        pairs.stream().forEach(i->System.out.println(i[0] + " " + i[1]));

        //Reduce
        Integer num = menu.stream().map(t->1).reduce(0, Integer::sum);


        Optional<Dish> maxCaloryDish = menu.stream().collect(Collectors.maxBy(Comparator.comparingInt(Dish::getCalories)));
    }




}
