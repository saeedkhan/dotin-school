package com.dotin.apple;

public class Orange extends Fruit {
    public Orange() {
    }

    public Orange(int weight) {
        super(weight);
    }

    public Orange(String color) {
        super(color);
    }

    public Orange(int weight, String color) {
        super(weight, color);
    }
}
