package com.dotin.apple;

import java.util.function.Function;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public class ParallelStreamsService {
    public static long sequentialSum(long l) {
        return Stream.iterate(1l, i -> i + 1).limit(l).sequential().reduce(Long::sum).get();
    }

    public static long parallelSum(long l) {
        return Stream.iterate(1l, i -> i + 1).limit(l).parallel().reduce(Long::sum).get();
    }

    public static long rangedSum(long l) {
        return LongStream.rangeClosed(0, l).reduce(0, Long::sum);
    }


    public static long rangedParallelSum(long l) {
        return LongStream.rangeClosed(0, l).parallel().reduce(0, Long::sum);
    }

    public static long iterator(long l) {
        long sum = 0;
        for (int i = 0; i < l; i++) {
            sum = i + sum;
        }
        return sum;
    }

    public static long measuringTime(Function<Long, Long> f, long l) {
        long fastest = Long.MAX_VALUE;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            f.apply(l);
            long duration = (System.nanoTime() - start) / 1000;
            if (duration < fastest) fastest = duration;
        }
        return fastest;
    }
}
