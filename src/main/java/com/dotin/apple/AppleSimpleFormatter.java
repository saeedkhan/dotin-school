package com.dotin.apple;

public class AppleSimpleFormatter implements AppleFormatter {
    public String accept(Apple ap){
        return "An apple of " + ap.getWeight() + "";
    }
}
