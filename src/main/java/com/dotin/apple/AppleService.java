package com.dotin.apple;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public interface AppleService {
    List<String> formatApples(List<Apple> apples);
    void appleSorter(List<Apple> inventory);
    String fileProcess(BufferedReaderProcessor bfr);
    <String> List<String> nonEmptyFinder(List<String> list, Predicate<String> p);
    <T> void printer(List<T> list, Consumer<T> c);
    <T, R> List<R> mapper(List<T> l, Function<T,R> f);
    List<Apple> map(List<String> list, Function<String, Apple> f);
    Fruit fruitFactory(String fruitName, int weight);

    static AppleService get() {
        return new AppleServiceImpl();
    }
}
