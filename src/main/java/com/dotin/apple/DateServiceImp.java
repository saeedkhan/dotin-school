package com.dotin.apple;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;

public class DateServiceImp implements DateService{

    @Override
    public Temporal getNextWorkingDay(Temporal temp){
        return temp.with(new NextWorkingDay());
    }

    public Temporal getNextWorkingDayV2(Temporal temp){
        return temp.with(temporal -> {
            DayOfWeek dow = DayOfWeek.of(temp.get(ChronoField.DAY_OF_WEEK));
            int daysToAdd = 1;
            if(dow == DayOfWeek.FRIDAY) daysToAdd = 3;
            else if(dow == DayOfWeek.SATURDAY) daysToAdd = 2;
            return temp.plus(daysToAdd, ChronoUnit.DAYS);
        });
    }

    public String dateFormatter(LocalDate localDate){
        return localDate.format(DateTimeFormatter.BASIC_ISO_DATE);
    }

    public LocalDate dateParser(String date){
        return LocalDate.parse(date, DateTimeFormatter.BASIC_ISO_DATE);
    }
}
